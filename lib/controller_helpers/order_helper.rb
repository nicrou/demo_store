module ControllerHelpers
  module OrderHelper
    extend ActiveSupport::Concern

    included do
      helper_method :current_order
    end

    # The current incomplete order for use in cart and during checkout
    def current_order(options = {})
      options[:create_order_if_necessary] ||= true

      @current_order = find_order_by_user(options, true)

      if options[:create_order_if_necessary] && (@current_order.nil?)
        @current_order = Order.create!(current_order_params.merge({status: 'cart'}))
      end

      @current_order
    end

    private

    def last_incomplete_order
      @last_incomplete_order ||= current_user.last_incomplete_order
    end

    def current_order_params
      { user_id: current_user.try(:id) }
    end

    def find_order_by_user(options = {}, with_adjustments = false)
      # Find any incomplete orders for the current user
      order = last_incomplete_order
      order
    end
  end
end
