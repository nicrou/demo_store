module ApplicationHelper
  def concat_options(h1, h2)
    h1.merge(h2) do |_, v1, v2| 
      v1.concat(" #{v2}")
    end
  end

  def material_icon_tag(name, options = nil, html_options = nil)
    html_options = concat_options({:class => 'material-icons'}, html_options ||= {})
    html_options = convert_options_to_data_attributes(options, html_options)
    content_tag :i, options, html_options do
      name.to_s
    end
  end
end
