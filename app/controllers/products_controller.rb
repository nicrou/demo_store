class ProductsController < ApplicationController
  include ControllerHelpers::OrderHelper
  before_action :authorize

  layout 'application'

  def index
    query = params[:query]
    if query.present?
      @products = Product.where("name LIKE ? or sku LIKE ?", "%#{query}%", "%#{query}%")
    else
      @products = Product.all
    end

    @order = current_order
  end
end
