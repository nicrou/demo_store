class ApplicationController < ActionController::Base
  helper_method :current_user
  include ControllerHelpers::OrderHelper
  protect_from_forgery

  private

  def current_user
    if session[:user_id]
      @current_user ||= User.select(:id, :email).find(session[:user_id])
    else
      @current_user = nil
    end
  end

  def authorize
    redirect_to login_url, alert: "Not authorized" if current_user.nil?
  end
end
