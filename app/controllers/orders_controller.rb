class OrdersController < ApplicationController
  before_action :authorize

  def edit
    @current_order = current_user.last_incomplete_order
  end

  def add_address
    @current_order = current_user.last_incomplete_order
    begin
      ActiveRecord::Base.transaction do
        if @current_order.address.present?
          @current_order.address.update!(address_params)
        else
          @address = Address.new(address_params)
          @current_order.address = @address if @address.save!
        end

        @current_order.save!
      end
    rescue ActiveRecord::RecordInvalid => exception
      @error_message = exception.message
    end

    respond_to do |format|
      format.js { render :edit }
    end
  end

  def populate
    if add_line_item
      respond_to do |format|
        format.js { render :populate }
      end
    end
  end

  def complete
    @current_order = current_user.last_incomplete_order
    @current_order.update!(completed_at: DateTime.now)
    OrdersMailer.complete(@current_order).deliver
    render :return
  end

  private

  def address_params
    params.require(:address).permit(:first_name, :last_name, :postal_code, :city,
                                    :street, :floor, :phone, :mobile_phone)
  end

  def line_item_params
    params.require(:line_item)
  end

  def add_line_item
    begin
      Order.transaction do
        product = Product.find(params[:product_id])
        line_item = LineItem.create!({order: current_order(create_order_if_necessary: true), product: product}) unless product.nil?
        line_item.order.update_adjustments
      end
    rescue
      return false
    end
  end
end
