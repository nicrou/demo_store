class LineItemsController < ActionController::Base
  before_action :authorize
  
  def destroy
    line_item = LineItem.find(params[:line_item_id])
    order = line_item.order

    if order && order.cart?
      if order.line_items.count == 1
        order.destroy
      else
        line_item.destroy
      end

      respond_to do |format|
        format.html { redirect_to cart_url }
        format.js   { }
      end
    end
  end

end
