class LineItem < ApplicationRecord
  belongs_to :order
  belongs_to :product, optional: true

  monetize :total_cents

  def create_variants(prototypes)
    prototypes.each do |prototype|
      Core::Variant.create(line_item: self, prototype: prototype)
    end
  end

  def update_adjustments

  end

  def price
    product.price
  end
end
