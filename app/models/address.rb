class Address < ApplicationRecord
  with_options presence: true do
    validates :first_name, :last_name
    validates :street, :postal_code
    validates :mobile_phone
  end

  def full_name
    "#{first_name} #{last_name}".strip if first_name && last_name
  end

  def to_s
    "#{street} #{postal_code} #{city}"
  end

  def name
    "#{self.mobile_phone} - #{self.full_name}, #{self.to_s}"
  end
end
