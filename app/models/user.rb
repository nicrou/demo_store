class User < ApplicationRecord
  has_secure_password
  validates :email, presence: true, uniqueness: true

  has_many :orders

  def last_incomplete_order
    orders.incomplete.order('created_at DESC').first
  end
end
