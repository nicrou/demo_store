class Order < ApplicationRecord
  include NumberGenerator.new(prefix: 'R')
  include TokenGenerator
  extend FriendlyId
  friendly_id :number, use: [:slugged, :finders]

  belongs_to :user, optional: true

  belongs_to :address, foreign_key: :address_id, class_name: 'Address', optional: true

  scope :created_between, -> (start_date, end_date) { where(created_at: start_date..end_date) }
  scope :completed_between, -> (start_date, end_date) { where(completed_at: start_date..end_date) }
  scope :complete, -> { where.not(completed_at: nil) }
  scope :incomplete, -> { where(completed_at: nil) }

  # shows completed orders first, by their completed_at date, then uncompleted orders by their created_at
  scope :reverse_chronological, -> { order(Arel.sql('orders.completed_at IS NULL'), completed_at: :desc, created_at: :desc) }

  has_many :line_items, :dependent => :destroy

  validates :number, presence: true, length: { maximum: 32, allow_blank: true }, uniqueness: { allow_blank: true }

  with_options presence: true do
    validates :address, if: :is_completed?
    validates :status
    validates :total, numericality: { greater_than: 0 }, if: :is_completed?
  end

  monetize :total_cents

  def self.statuses
    %w(cart processing paid delivered)
  end

  self.statuses.each do |status|
    define_method :"#{status}?" do |*args|
      self.status.eql? status
    end
  end

  def is_completed?
    self.completed_at.present?
  end

  def update_adjustments
    self.total = 0
    self.line_items.each do |li|
      self.total += li.product.price
    end
    self.save!
  end

  # Associates the specified user with the order.
   def associate_user!(user, override_email = true)
     self.user           = user
     self.save!
   end
end
