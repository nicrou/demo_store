class Product < ApplicationRecord
  extend FriendlyId
  friendly_id :sku, use: :slugged

  monetize :price_cents
end
