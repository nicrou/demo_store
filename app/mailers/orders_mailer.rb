class OrdersMailer < ApplicationMailer
  def complete(order)
    @order = order
    mail(
      to: order.user.email,
      subject: "Order #{order.number} completed"
    ) do |format|
      format.html { render 'orders_mailer/complete'}
    end
  end
end
