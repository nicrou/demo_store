class ApplicationMailer < ActionMailer::Base
  default from: 'nicroumeliotis91@gmail.com'
  layout 'mailer'
end
