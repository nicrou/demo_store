Rails.application.routes.draw do

  resources :products, only: [:index]

  root to: "products#index"

  post 'populate/:product_id', to: 'orders#populate', as: 'populate'

  resources :orders, only: [:update]
  get  'order/:order_id', to: 'orders#edit', as: 'checkout'
  post 'orders/:order_id/add_address', to: 'orders#add_address', as: 'add_address'
  post 'orders/:order_id/complete', to: 'orders#complete', as: 'complete'

  post 'line_items/:line_item_id/delete', to: 'line_items#destroy'

  resources :products
  resources :sessions, only: [:new, :create, :destroy]

  get 'login', to: 'sessions#new', as: 'login'
  get 'logout', to: 'sessions#destroy', as: 'logout'
end
