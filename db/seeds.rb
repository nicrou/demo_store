# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Product.create([{ name: 'Lord of the Rings - The Fellowship of the Ring', price: 13, invetory: 35 },
                { name: 'Lord of the Rings - The Two Towers', price: 13, invetory: 35 },
                { name: 'Lord of the Rings - The Return of the King', price: 15, invetory: 35 },
                { name: 'The Wheel of Time - New Spring', price: 17, invetory: 39 },
                { name: 'The Wheel of Time - The Eye of the World', price: 17, invetory: 19 },
                { name: 'The Wheel of Time - The Great Hunt', price: 17, invetory: 34 },
                { name: 'The Wheel of Time - The Dragon Reborn', price: 17, invetory: 39 },
                { name: 'The Wheel of Time - The Shadow Rising', price: 17, invetory: 56 },
                { name: 'The Wheel of Time - The Fires of Heaven', price: 17, invetory: 12 },
                { name: 'The Wheel of Time - Lord of Chaos', price: 17, invetory: 43 },
                { name: 'The Wheel of Time - A Crown of Swords', price: 18, invetory: 63 },
                { name: 'The Wheel of Time - The Path of Daggers', price: 17, invetory: 5 },
                { name: 'The Wheel of Time - Winter\'s Heart', price: 17, invetory: 12 },
                { name: 'The Wheel of Time - Crossroads of Twilight', price: 19, invetory: 43 },
                { name: 'The Wheel of Time - Knife of Dreams', price: 17, invetory: 63 },
                { name: 'The Wheel of Time - The Gathering Storm', price: 20, invetory: 5 },
                { name: 'The Wheel of Time - Towers of Midnight', price: 20, invetory: 63 },
                { name: 'The Wheel of Time - A Memory of Light', price: 23, invetory: 5 },
                { name: 'Prince of Thorns', price: 8.3, invetory: 17 },
                { name: 'King of Thorns', price: 12, invetory: 10 },
                { name: 'Emperor of Thorns', price: 13, invetory: 35 },
                { name: 'Mistborn - The Final Empire', price: 17, invetory: 39 },
                { name: 'Mistborn - The Well of Ascension', price: 8.3, invetory: 17 },
                { name: 'Mistborn - The Hero of Ages', price: 9.5, invetory: 25 }])

Product.find_each(&:save)
User.create([{ email: 'nicroumeliotis91@gmail.com', password: '654321', admin: true}])
User.create([{ email: 'customer@service.com', password: '123456'}])
