class CreateLineItems < ActiveRecord::Migration[5.2]
  def change
    create_table :line_items do |t|
      t.references :order
      t.references :product
      t.monetize   :total,                null: false

      t.timestamps null: false
    end

    add_index :line_items, [:order_id], name: 'index_line_items_on_order'
  end
end
