class CreateAddresses < ActiveRecord::Migration[5.2]
  def change
    create_table :addresses do |t|
      t.string     :friendly_name
      t.string     :first_name
      t.string     :last_name
      t.string     :city
      t.string     :street
      t.string     :floor
      t.string     :phone
      t.string     :mobile_phone
      t.string     :postal_code

      t.timestamps null: false
    end
    add_index :addresses, [:first_name], name: 'index_addresses_on_firs_tname'
    add_index :addresses, [:last_name],  name: 'index_addresses_on_last_name'
  end
end
