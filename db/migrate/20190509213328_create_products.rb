class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.string :sku
      t.string :name
      t.text   :description

      t.monetize :price, null: false

      t.boolean :published, default: true
      t.boolean :available, default: true
      t.integer :invetory

      t.timestamps null: false
    end
  end
end
