class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.string     :number,               limit: 15
      t.monetize   :total,                null: false
      t.string     :status
      t.references :user
      t.references :address
      t.datetime   :completed_at

      t.timestamps null: false
    end

    add_index :orders, [:number], name: 'index_orders_on_number'
  end
end
